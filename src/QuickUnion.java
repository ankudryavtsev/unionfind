public class QuickUnion extends UF
{
    public QuickUnion(int N)
    {
        super(N);
    }

    @Override
    public void union(int p, int q)
    {
        int pRoot = findRoot(p);
        int qRoot = findRoot(q);
        if(pRoot == qRoot)
            return;
        id[pRoot] = qRoot;
        count--;
    }

    @Override
    public int find(int p)
    {
        return findRoot(p);
    }

    protected int findRoot(int p)
    {
        while(p != id[p])
            p = id[p];
        return p;
    }
}
