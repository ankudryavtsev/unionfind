import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public abstract class UFTestCases
{
    protected UF algorithm;

    /**
     * Sets instance to a newly constructed algorithm of given extent.
     *
     * @param n extent
     */
    protected abstract void setInstance(final int n);

    private Random r = new Random();

    @Test
    public void testConstructor()
    {
        int size = 10;
        setInstance(size);
    }

    private void testConnection(int a, int b)
    {
        System.out.println("testConnection(" + a + ", " + b + ")");
        algorithm.union(a, b);
        assertTrue(algorithm.connected(a, b));
    }

    @Test
    public void testSimpleUnion()
    {
        setInstance(100);
        int a, b;
        for(int i=0;i<10;i++)
        {
            a = r.nextInt(100);
            b = r.nextInt(100);
            testConnection(a, b);
        }
    }

    @Test
    public void testСomplexUnion()
    {
        setInstance(100);
        int a = r.nextInt(100);
        int b = r.nextInt(100);
        int c = r.nextInt(100);
        System.out.println("testComplexUnion(" + a + ", " + b + ", " + c + ")");
        algorithm.union(a, b);
        algorithm.union(b, c);
        assertTrue(algorithm.connected(a, c));
    }

    @Test
    public void testFalseConnection()
    {
        setInstance(10);
        int a = r.nextInt(10);
        int b = r.nextInt(10);
        assertFalse(algorithm.connected(a, b-1));
    }

    @Test
    public void testCount()
    {
        setInstance(10);
        algorithm.union(1, 2);
        algorithm.union(2, 3);
        algorithm.union(3, 4);
        assertEquals(7, algorithm.count());
    }
}
